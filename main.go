package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path"
	"runtime/pprof"

	"gitlab.com/xen-project/misc/session-scheduler/event"
	"gitlab.com/xen-project/misc/session-scheduler/id"
	"gitlab.com/xen-project/misc/session-scheduler/keyvalue"
	"gitlab.com/xen-project/misc/session-scheduler/timezones"
)

var kvs *keyvalue.KeyValueStore

const (
	KeyDefaultLocation = "EventDefaultLocation"
	VerificationCode   = "ServeVerificationCode"
)

var DefaultLocation = "Europe/Berlin"

var DefaultLocationTZ event.TZLocation

var TimezoneList []string

// Template and data code expect CWD to be in the same directory as
// the binary; make this so.
func cwd() {
	execpath, err := os.Executable()
	if err != nil {
		log.Printf("WARNING: Error getting executable path (%v), cannot cd to root", err)
		return
	}

	execdir := path.Dir(execpath)
	log.Printf("Changing to directory %s", execdir)

	err = os.Chdir(execdir)
	if err != nil {
		log.Printf("WARNING: Chdir to %s failed: %v", execdir, err)
		return
	}

	// Create the "data" directory if it doesn't exist
	dataDir := "data"
	err = os.MkdirAll(dataDir, 0755) // 0755 is the permission mode
	if err != nil {
		log.Fatalf("Failed to create directory %s: %v", dataDir, err)
	}
	log.Printf("Ensured directory %s exists", dataDir)
}

func main() {
	var err error

	TimezoneList, err = timezones.GetTimezoneList()
	if err != nil {
		log.Fatalf("Getting timezone list: %v", err)
	}

	cwd()

	templatesInit()

	kvs, err = keyvalue.OpenFile("data/serverconfig.sqlite")
	if err != nil {
		log.Fatalf("Opening serverconfig: %v", err)
	}

	adminPwd := flag.String("admin-password", "", "Set admin password")

	flag.Var(kvs.GetFlagValue(KeyServeAddress), "address", "Address to serve http from")
	flag.Var(kvs.GetFlagValue(KeyDefaultLocation), "default-location", "Default location to use for times")
	flag.Var(kvs.GetFlagValue(LockingMethod), "servelock", "Server locking method.  Valid options are none, quit, wait, and error (default quit)")

	cpuprofile := flag.String("cpuprofile", "", "write cpu profile to `file`")

	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(),
			`Usage:

 %s <options> <command>

This is a general-purpose tool for managing unconference-style session
schedulers.

Command-line options can be classified into local-invocation settings,
permanent settings, and actions.

PERMANENT SETTINGS

Setting any of the following options will cause perisistent changes in
the event or server config databases.  Future invocations will use the
stored changes by default.

  -admin-password <password>
        Set the admin password.
  -address <listen_address>
        Host and port to serve http from.  Example: "localhost:20938"
  -default-location <timezone>
        Default time zone to use for new accounts (typically the
        timezone of the event).  Example: "Europe/London"
  -servelock
        How to respond if another instance of the server claims to be
        running on the same lockfile.  Options are
         - "none": don't access the lockfile
         - "quit": exit without an error code if another instance is
           running (default)
         - "wait": wait for the other server to exit
         - "error": exit with an error code if another instance is
           running

LOCAL SETTINGS

The following settings only affect the current invocation of the
program.

   -cpuprofile <filename>
        This will write golang cpuprofile information the given file.
        It can be analyzed with 'go tool pprof'.

ACTIONS

If an action is specified, only that action will be taken.  If no
action is specified, the default is "serve".

  serve
        Start up an http server listening on <listen_address> above.
        If no address has been previously specified, a port will be
        chosen at random and stored in the permanent configuration.
  editTimetable
        Run $EDITOR with a YAML based version of the timetable (or an
        empty timetable if none exists.
  schedule
        Run the scheduler
`, os.Args[0])

	}

	flag.Parse()

	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal("could not create CPU profile: ", err)
		}
		if err := pprof.StartCPUProfile(f); err != nil {
			log.Fatal("could not start CPU profile: ", err)
		}
		defer pprof.StopCPUProfile()
	}

	{
		_, err := kvs.Get(VerificationCode)
		switch {
		case err == keyvalue.ErrNoRows:
			vcode := id.GenerateRawID(8)
			if err = kvs.Set(VerificationCode, vcode); err != nil {
				log.Fatalf("Setting Event Verification Code: %v", err)
			}
		case err != nil:
			log.Fatalf("Getting Event Verification Code: %v", err)
		}
	}

	locstring, err := kvs.Get(KeyDefaultLocation)
	switch {
	case err == keyvalue.ErrNoRows:
		locstring = DefaultLocation
		if err = kvs.Set(KeyDefaultLocation, locstring); err != nil {
			log.Fatalf("Setting default location: %v", err)
		} else {
			log.Printf("Default location to %s", DefaultLocation)
		}
	case err != nil:
		log.Fatalf("Getting default location: %v", err)
	}

	DefaultLocation = locstring
	DefaultLocationTZ, err = event.LoadLocation(locstring)
	if err != nil {
		log.Fatalf("Couldn't load location %s: %v", locstring, err)
	}

	err = event.Load(event.EventOptions{AdminPwd: *adminPwd, DefaultLocation: locstring})
	if err != nil {
		log.Fatalf("Loading schedule data: %v", err)
	}

	cmd := flag.Arg(0)
	if cmd == "" {
		cmd = "serve"
	}

	switch cmd {
	case "serve":
		serve()
	case "schedule":
		MakeSchedule(false)
	case "editTimetable":
		EditTimetable()
	default:
		log.Fatalf("Unknown command: %s", cmd)
	}

}

func MakeSchedule(async bool) error {
	return event.MakeSchedule()
}
